FROM node:alpine

WORKDIR ~/workspace/nodejs-apirest-cluster

COPY . .

RUN npm install
EXPOSE 8080
CMD npm start
