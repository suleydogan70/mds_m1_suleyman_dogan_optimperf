# mds_m1_suleyman_dogan_optimperf

## Pré-requis

* npm 6.9.0^
* nodejs 10.15.3^
* mongodb 4.0.3 ou un compte sur mongodb Atlas

## J'ai un compte MongoDB Atlas

* Cliquer dans le menu latéral gauche sur `clusters` puis sur le bouton `Build a new cluster`.
* Ne pas toucher à la configuration gloable. Pour le provider et la région, cocher `AWS` et `N. Virginia` dans North America.
* Si vous voulez la version gratuite, dans `Cluster Tier`, cocher le `M0 Sandbox` qui est premier dans la liste. Sinon cocher par rapport à vos besoins.
* Pour finir, appelez votre cluster `cluster0` puis cliquer sur `Create Cluster`. Attendre un instant avant le déploiment du cluster.

* Ensuite cliquer sur `Database Access` pour créer un nouvel utilisateur.
* Cliquer sur `add new user`, lui donner les permissions de lire et écrire sur toutes les tables : `Read and write to any database`
* Puis lui créer un nom d'utilisateur et un mot de passe.
* Une fois fait, copier le nom de l'utilisateur et le mot de passe dans le fichier `./config/identifications.js` pour pouvoir ensuite effectuer la connexion.
* Continuer ensuite en cliquant sur `Network Access` pour donner les droits de connexion.
* Cliquer sur `add ip address` puis cliquer sur `Allow access from anywhere`, et `Confirm`.
* Maintenant que toutes les options sont prêtes, rediriger vous sur `Clusters`.
* Cliquer sur `Connect` dans les boutons du cluster, et ensuite cliquer sur `Connect Your Application`.
* Copier le lien dans `Connection String Only`, puis coller le dans le fichier `./config/connection.js` dans la constante `dbLink`.
* Mettre entre les `//` et le `@` du lien : `//${username}:${password}@` ce qui va permettre de récupérer le nom de compte et le mot de passe de connection à la base.

Maintenant que tout est prêt, vous pouvez lancer l'application.

## Replica set en local

### Lancer une instance mongod

Pour commencer, créer un dossier dans `/var/lib/` du nom de `mongodb1` par exemple.

Si vous n'êtes pas sur linux, vous devez créer ce dossier dans le dossier où sont stockés les données de mongo.

Puis ensuite, lancer une instance mongod :
```
  > mkdir /var/lib/mongodb1
  > mongod --port 27017 --dbpath /var/lib/mongodb1 --replSet rs0
```
Ici, on lance une instance sur le port 27017, les données seront stockées dans le dossier `/var/lib/mongodb1`, et on lie cette instance au replica set `rs0`.

Répéter l'action 2 fois de plus en changeant le port et le dossier :
```
  > mkdir /var/lib/mongodb2
  > mongod --port 27018 --dbpath /var/lib/mongodb2 --replSet rs0
  > mkdir /var/lib/mongodb3
  > mongod --port 27019 --dbpath /var/lib/mongodb3 --replSet rs0
```

### Commencer la réplication

Tout d'abord, lancer un shell mongo :
```
  > mongo
```
Ce shell va directement se connecter sur l'instance sur le port 27017 car c'est le port par défaut de mongodb.

Ensuite taper `rs.initiate()` pour initialiser la réplication.

Normalement votre shell affiche maintenant `rs0:PRIMARY>`.

Si ce n'est pas le cas, ce n'est pas grave, les prochaines manipulations règleront ce problème.

Maintenant il faut ajouter les autres instances en tapant :
```
  > rs.add('localhost:27018')
  > rs.add('localhost:27019')
```
Après ça, vous pouvez taper `rs.status()`, ce qui va vous montrer dans `members`, les 3 instances avec leurs status (PRIMARY / SECONDARY).

Maintenant votre réplication est fonctionnelle. Pour la tester vous pouvez ajouter des données :
```
  > use students
  > db.student.insertOne({ studentName: "Suleyman", studentAge: 21})
```
Puis dans une nouvelle console, lancer :
```
  > mongo --port 27018
  > use students
  > db.student.find()
```
Si cela ne fonctionne pas, taper dans le shell mongo `rs.slaveOk()` pour permettre à la SECONDARY de pouvoir faire des requêtes, puis réessayer.

## Lancer l'application

```
  > git clone https://gitlab.com/suleydogan70/mds_m1_suleyman_dogan_optimperf.git
  > cd mds_m1_suleyman_dogan_optimperf
  > npm i
```

L'application se lance de base sur mongodb Atlas.

Pour modifier ça, dans le fichier `./config/connection.js`, changer la constante `local` à true, puis lancer l'application :

```
  > npm start
```

## API

`GET http://localhost:3000/student`

Liste tous les étudiants de la base de données.

`POST http://localhost:3000/student`

Insère un étudiant dans la base de données.

Arguments :

* studentName (String)[required]
* studentAge (Number)[required]
* studentFuturJob (String)[optional]

`GET http://localhost:3000/student/:studentid`

Affiche l'étudiant en question.

`PUT http://localhost:3000/student/:studentid`

Mets à jour l'étudiant.

Arguments :

* studentName (String)[required]
* studentAge (Number)[required]
* studentFuturJob (String)[optional]

`DELETE http://localhost:3000/student/:studentid`

Supprime l'étudiant en question.

## Lancer les tests

```
  > npm test
```

## Architecture de l'application

![Architecture de l'application](./architecture.JPG)
