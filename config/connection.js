const mongoose = require('mongoose')
const { username, password } = require('./identifications')
const local = false
const dbLink =
  local
  ? `mongodb://localhost:27017,localhost:27018,localhost:27019/mongodb1?replicaSet=rs0`
  : `mongodb+srv://${username}:${password}@cluster0-6zpks.mongodb.net/test?retryWrites=true&w=majority`

const dbOptions = {
  useNewUrlParser: true,
  poolSize: 10,
}

mongoose.connect(dbLink, dbOptions)
  .then(() => {
    console.log('DB connected !')
  },
  err => {
    console.log('Error db connection ', err)
  }
)

require('../models/Student')
