const mongoose = require('mongoose')
const Schema = mongoose.Schema

const StudentSchema = new Schema({
  studentName: {
    type: String,
    required: true
  },
  studentAge: {
    type: Number,
    required: true
  },
  studentFuturJob: {
    type: String
  }
})

module.exports = mongoose.model('Students', StudentSchema)
