const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const db = require('./config/connection')
const studentController = require('./controllers/StudentController')
const port = process.env.PORT || 3301

/* Middlewares */

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

/* Endpoints */
app
  .route("/student")
  .get(studentController.listAllStudents)
  .post(studentController.createNewStudent)

app
  .route("/student/:studentid")
  .get(studentController.readStudent)
  .put(studentController.updateStudent)
  .delete(studentController.deleteStudent)

/* Server */

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`)
})
