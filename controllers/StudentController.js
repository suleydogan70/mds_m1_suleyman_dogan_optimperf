const Student = require('../models/Student')

module.exports = {
  createNewStudent(req, res) {
    let newStudent = new Student(req.body)
    newStudent.save((err, student) => {
      if (err) {
        res.status(500).send(err)
      }
      res.status(201).json(student)
    })
  },
  readStudent(req, res) {
    Student.findById(req.params.studentid, (err, student) => {
      if (err) {
        res.status(500).send(err)
      }
      res.status(200).json(student)
    })
  },
  updateStudent(req, res) {
    Student.findOneAndUpdate(
      { _id: req.params.studentid },
      req.body,
      { new: true },
      (err, student) => {
        if (err) {
          res.status(500).send(err)
        }
        res.status(200).json(student)
      }
    )
  },
  deleteStudent(req, res) {
    Student.remove({ _id: req.params.studentid }, (err, student) => {
      if (err) {
        res.status(404).send(err)
      }
      res.status(200).json({ message: 'Student successfully deleted' })
    })
  },
  listAllStudents(req, res) {
    Student.find({}, (err, student) => {
      if (err) {
        res.status(500).send(err)
      }
      res.status(200).json(student)
    })
  }
}
