const chai = require('chai')
const chaiHttp = require('chai-http')
const app = `http://localhost:${process.env.PORT || 3301}`
let studentId

chai.should()
chai.use(chaiHttp)

describe('student API', () => {
  it('POST /student should create an new student', done => {
    const payload = {
      studentName: 'testStudent',
      studentAge: 22
    }

    chai.request(app)
      .post('/student')
      .send(payload)
      .end((err, res) => {
        res.should.have.status(201)
        studentId = res.body._id
        done()
      })
  })

  it('POST /student should create an new student with error', done => {
    const payload = {
      studentName: 'testStudent'
    }

    chai.request(app)
      .post('/student')
      .send(payload)
      .end((err, res) => {
        res.should.have.status(500)
        done()
      })
  })

  it('GET /get all students', done => {

    chai.request(app)
      .get('/student')
      .end((err, res) => {
        res.should.have.status(200)
        done()
      })
  })

  it('GET /get a given student', done => {

    const id = studentId
    const studentName = 'testStudent'

    chai.request(app)
      .get(`/student/${id}`)
      .end((err, res) => {
        res.should.have.status(200)
        const result = JSON.parse(res.text)
        result.studentName.should.be.eql(studentName)
        result._id.should.be.eql(id)
        done()
      })
  })
})
